module SessionsHelper
    # Log in the given user
    def log_in(user)
       session[:user_id] = user.id 
    end
    
    #log out the given user
    def log_out
       forget_persistent_session(current_user)
       session.delete(:user_id)
       @current_user = nil
    end
    
    # Remember a user in a persistent session
    def remember_persistent_session(user)
        user.remember
        cookies.signed.permanent[:user_id] = user.id
        cookies.permanent[:remember_token] = user.remember_token 
    end
    # Forget a user in a  persistence session
    def forget_persistent_session(user)
       user.forget
       cookies.delete(:user_id)
       cookies.delete(:remember_token)
    end
    
    # Returns true if the given user equals the current user
    def current_user?(user)
       user == current_user 
    end
        
    
    # Returns the current logged-in user (if any)
    def current_user
       if session[:user_id]
         @current_user ||= User.find_by(id: session[:user_id])
       elsif cookies.signed[:user_id]
         user = User.find_by(id: cookies.signed[:user_id])
         if user && user.authenticated?(:remember, cookies[:remember_token])
             log_in(user)
             @current_user = user
         end
       end
    end
    
    # Alternate implementation of the 'current_user' method
    # def current_user
    #   if(user_id = session[:user_id])
    #       @current_user ||= User.find_by(id: user_id)
    #   else(user_id = cookies.signed[:user_id]) 
    #       user = User.find_by(id: user_id)
    #       if user && user.authenticated?(cookies[:remember_token])
    #           log_in user
    #           @current_user = user
    #       end
    #   end
    # end
    
    # Returns true if the user is logged in, and false otherwse
    # is_logged_in? == logged_in?
    def is_logged_in?
        !current_user.nil?
    end 
    
   
    # Redirects back to stored location or (to default)
    def redirect_back_or(default)
       redirect_to session[:forwading_url] || default
       session.delete(:forwading_url)
    end
    
     # Stores the URL of the location trying to be accessed
    def store_location
       session[:forwading_url] = request.original_url if request.get? 
    end
    
end
