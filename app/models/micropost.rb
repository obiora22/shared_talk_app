class Micropost < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true 
  validates :content, presence: true, length: {maximum: 140}
  validate :picture_size
  #default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  # Same as above
  def self.default_scope
     order(created_at: :desc) 
  end
  
  private 
  
  # Validates size of uploaded pictures 
  def picture_size 
    if picture.size > 5.megabytes
      errors.add(:picture, "Should be less than 5MB")
    end
  end
  
end
