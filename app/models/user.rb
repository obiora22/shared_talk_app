class User < ActiveRecord::Base
    # before_save {self.email = email.downcase}
    
    # Method reference--compare with explicit block above
    before_save :downcase_email
    
    before_create :create_activation_digest
    
    # Adds 'password' & 'password_confirmation' virtual attributes and 'authentication' method
    has_secure_password 
    
    VALID_EMAIL_PATTERN = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    
    validates :email, presence: true, length: {maximum: 255},format: {with: VALID_EMAIL_PATTERN}, uniqueness: {case_sensitive: false}
    validates :name, presence: true, length: {maximum: 50}
    validates :password, presence: true, length: {minimum: 6}, allow_nil: true # allows nil password entry for editing user. 'has_secure_password' has  a presence validation feature for when users sign up
    attr_accessor :remember_token, :activation_token, :reset_token
    
    has_many :microposts, dependent: :destroy
    has_many :active_relationships, class_name: "Relationship", foreign_key: "follower_id", dependent: :destroy 
    has_many :following, through: :active_relationships, source: :followed
    
    has_many :passive_relationships, class_name: "Relationship", foreign_key: "followed_id", dependent: :destroy
    has_many :followers, through: :passive_relationships, source: :follower
        

    # Returns hash digest when given string
    def User.digest(string)
       cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
       BCrypt::Password.create(string, cost: cost)
    end
    
    # Return a base64_url hash
    def User.new_token 
       SecureRandom.urlsafe_base64 
    end
    
    # Update user remember_digest attribute with hashed(using BCrypt) remember token 
    def remember
        self.remember_token = User.new_token 
        update_attribute(:remember_digest, User.digest(remember_token))
    end
    
    # Forgets a user
    def forget
       update_attribute(:remember_digest, nil) 
    end
    
    # Returns true if the given token matches the digest
    def authenticated?(attribute,token)
        digest = self.send("#{attribute}_digest")
        return false if digest.nil?
       # BCrypt::Password.new(remember_digest).is_password?(remember_token) 
       BCrypt::Password.new(digest).is_password?(token)
    end
    
    # Activates account
    def activate
       update_attribute(:activated, true)
       update_attribute(:activated_at, Time.zone.now)
    end
    
    # Sends activation email
    def send_activation_email
       UserMailer.account_activation(self).deliver_now 
    end
    
    # Sets the password reset attribute
    def create_reset_digest
       self.reset_token = User.new_token
       update_attribute(:reset_digest, User.digest(reset_token))
       update_attribute(:reset_sent_at, Time.zone.now)
    end
    
    # Sends password reset email
    def send_password_reset_email
       UserMailer.password_reset(self).deliver_now 
    end
    
    # Returns true if password eset token has expired
    def password_reset_expired?
       reset_sent_at < 2.hours.ago
  
    end
    
    # Defines a proto-feed
    # See 'following users' for teh full implementation
    
    def feed
       #Micropost.where('user_id = ?',self.id)  
       #microposts
       #Ensures the variable 'id' is properly escaped before being included in the SQL query
       #Micropost.where('user_id IN (?) OR user_id= ?',following_ids,id) 
       following_ids = "SELECT followed_id FROM relationships WHERE follower_id = :user_id"
       Micropost.where("user_id IN (#{following_ids}) OR user_id = :user_id",user_id: id)
    end
    
    # Follow a user 
    def follow(other_user)
       active_relationships.create(followed_id: other_user.id) 
    end
    
    #  Unfollow a user 
    def unfollow(other_user)
       active_relationships.find_by(followed_id: other_user.id).destroy  
    end
    
    # Returns true if current user is following other user
    def following?(other_user)
       self.following.include?(other_user) 
    end
    private
    
    # To avoid database adapter incompatibility, downcase email address before saving to database
    def downcase_email
       self.email = self.email.downcase 
    end
    
    # Creates and signs the activation token & digest
    def create_activation_digest
       self.activation_token = User.new_token 
       self.activation_digest = User.digest(activation_token)
    end
end
