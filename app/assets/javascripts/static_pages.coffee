# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


# Slide toggle on :hover for 'Account" link 

$(document).on "ready page:change", ->
    #$(this).find('.dropdown-menu').first().slideUp()
    $('.navbar-nav .dropdown').hover( 
       -> $(this).find('.dropdown-menu').first().delay(250).slideDown()
       -> $(this).find('.dropdown-menu').first().delay(100).slideUp()
     )       
    
$ ->
  $('#micropost_picture').on 'change', ->
    size_in_megabytes = this.files[0].size/1024/1024
    if size_in_megabytes > 5 
      alert('Maximum file is 5MB. Please choose a smaller file size.')
   
       