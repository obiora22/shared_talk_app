class UsersController < ApplicationController
  
  before_action :logged_in_user, only: [:edit, :update, :index, :destroy,:followers,:following]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: :destroy
  
  def new
    @user = User.new
  end
  
  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page], per_page: 20)
  end
  
  def  index
    # Paginate results explicitly using 'paginate' method
    @users = User.paginate(page: params[:page],per_page: 15)
  end
  
  def create 
    @user = User.new(user_params)
    if @user.save
      # log_in(@user)
      # flash[:success] = 'Welcome to SharedTalk!'
      # redirect_to @user
      @user.send_activation_email
      flash[:success] = 'Please check your email to activate your account.'
      redirect_to root_url 
    else
      render 'new'
    end  
  end
  
  def edit
    
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile Updated"
      redirect_to @user
    else
      render 'edit'
    end  
  end
  
  def destroy
    @user = User.find(params[:id]).destroy
    flash[:success] = 'User Deleted'
    redirect_to users_url 
  end
  
  def following 
    @title = 'Following'
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end
  
  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  private
    
    # BEFORE FILTERS
    
    # Confirms a user is logged in
     # logged_in_user moved to Application_controller.rb
    
    # Confirms a user
    def correct_user
      @user = User.find(params[:id])
      redirect_to root_url unless current_user?(@user)
      # Alternative implementatio
      # redirect_to root_url unless @user == current_user
    end
    
    # Confirms user is admin
    def admin_user
      redirect_to root_url unless current_user.admin?
    end
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
end
