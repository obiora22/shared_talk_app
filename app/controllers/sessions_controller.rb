class SessionsController < ApplicationController
  
  def new
  end
  
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # Checks if user is activated
      if user.activated?
        #Log in user and redirect to user's show page
        log_in(user)
        params[:session][:remember_me] == '1' ? remember_persistent_session(user) : forget_persistent_session(user)
        # session_helper method
        redirect_back_or(user)
        # redirect_to user_path(user)
        # redirect_to user -> more compact form of the above code
      else
        message = 'Account not activated'
        message+= 'Check your email for the activation link'
        flash[:warning] = message
        redirect_to root_url 
      end
    else
      flash.now[:danger] = "Invalid user email/password information!"
      render 'new'
    end
  end
  
  def destroy
    log_out if is_logged_in?
    redirect_to root_url
  end
end
