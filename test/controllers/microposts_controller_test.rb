require 'test_helper'

class MicropostsControllerTest < ActionController::TestCase
  def setup 
    @micropost = microposts(:post1)
  end
  
  test 'should redirect create when not logged in' do 
    assert_no_difference 'Micropost.count', 1 do 
      post :create, micropost: {content: "Lorem ipsum"}
    end
    assert_redirected_to login_url 
  end
  
  test 'should redirect destroy when not logged in' do 
    assert_no_difference 'Micropost.count', -1 do 
      delete :destroy, id: @micropost
    end
    assert_redirected_to login_url
  end 
  
  test 'should redirect destroy for wrong post' do 
    log_in(users(:one))
    micropost = microposts(:post5)
    assert_no_difference 'Micropost.count' do 
      delete :destroy, id: micropost
    end
    assert_redirected_to root_url 
  end
end
