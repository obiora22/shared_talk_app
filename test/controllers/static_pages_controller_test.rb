require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  
  def setup
    @base_title = "SharedTalk"
  end
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", full_title('Home') # Using 'full_title' helper from the 'ApplicationHelper' module
  end

  test "should get help" do
    get :help
    assert_response :success
    assert_select "title", full_title('Help')
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", full_title('About')
  end
  
  test "should get contact"do 
    get :contact
    assert_response :success
    assert_select "title", "Contact | #{@base_title}" # Using '@base_title' from the 'setup' method 
  end
end
