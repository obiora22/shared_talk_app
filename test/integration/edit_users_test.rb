require 'test_helper'

class EditUsersTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:one)
    
  end
  
  test 'unsuccessful edit' do 
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: {name: 'obi', email: 'obi@example.com', password: 'foo', password_confirmation: 'foobar'} 
    assert_template 'users/edit'
  end
  
  # test 'successful edit' do
  #   #log_in_as(@user)
  #   get edit_user_path(@user)
  #   assert_template 'users/edit'
  #   patch user_path(@user), user: {name: 'obi', email: 'obi@example.com', password: '', password_confirmation: ''}
  #   assert_redirected_to @user
  #   @user.reload
  #   assert_equal 'obi', @user.name
  #   assert_equal 'obi@example.com', @user.email
  # end
  
  test 'successful edit with friendly forwarding' do 
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user) 
    patch user_path(@user), id: @user, user: {name: 'obiwon', email: 'obiwon@example.com', password: '', password_confirmation: ''}
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal 'obiwon', @user.name
    assert_equal 'obiwon@example.com', @user.email
  end
end
