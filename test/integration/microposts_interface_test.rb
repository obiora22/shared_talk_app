require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:two)
  end
  
  test 'micropost interface' do 
    log_in_as(@user)
    get root_path
    assert_select 'div.pagination'
    # Invalid submission
    assert_no_difference 'Micropost.count' do 
      post microposts_path, micropost: {content: "" }
    end
    assert_select 'div#error-explanation'
    # Valid submission
    message = " I am never going to give up"
    assert_difference 'Micropost.count' do 
      post microposts_path, micropost: { content: message}
    end
    assert_redirected_to root_url 
    follow_redirect!
    assert_match message, response.body 
    # Delete a post 
    assert_select 'a', text: 'Delete'
    first_micropost = @user.microposts.paginate(page: 1).first
    assert_difference 'Micropost.count', -1 do 
      delete micropost_path(first_micropost)
    end
    # Visit a different user 
    get user_path(users(:one))
    assert_select 'a', text: 'Delete', count: 0
    end
end
