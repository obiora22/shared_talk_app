require 'test_helper'

class MicropostTest < ActiveSupport::TestCase
  
  def setup 
    @user = users(:one)
    @micropost = @user.microposts.build(content: "Lorem ipsum")
  end
  
  test 'valid micropost' do 
    assert @micropost.valid? 
  end
  
  test 'user id should be present' do 
    @micropost.user_id = nil 
    assert_not @micropost.valid?
  end 
  
  test 'content should be present' do 
    @micropost.content = nil 
    assert_not @micropost.valid?
  end
  
  test 'content length should be less than 140' do 
    @micropost.content = 'a' * 141
    assert_not @micropost.valid?
  end  
  
  test 'order must be most recent first' do 
    assert_equal microposts(:post4), Micropost.first
  end
end
